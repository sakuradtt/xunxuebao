







package io.renren.modules.oss.cloud;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import io.renren.common.exception.RRException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 阿里云存储
 *
 * @author Mark sunlightcs@gmail.com
 */
public class AliyunCloudStorageService extends CloudStorageService {

    private OSSClient client;

    public AliyunCloudStorageService(CloudStorageConfig config){
        this.config = config;

        //初始化
        init();
    }

    private void init(){
        client = new OSSClient(config.getAliyunEndPoint(), config.getAliyunAccessKeyId(),
                config.getAliyunAccessKeySecret());
    }

    @Override
    public String upload(byte[] data, String path) {
        return upload(new ByteArrayInputStream(data), path);
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        try {
            if(! client.doesBucketExist(config.getAliyunBucketName())){
                client.createBucket(config.getAliyunBucketName());
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(config.getAliyunBucketName());
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicReadWrite);
                client.createBucket(createBucketRequest);
            }
            client.putObject(config.getAliyunBucketName(), path, inputStream);
            //设置权限 这里是公开读
            client.setBucketAcl(config.getAliyunBucketName(),CannedAccessControlList.PublicReadWrite);

        } catch (Exception e){
            throw new RRException("上传文件失败，请检查配置信息", e);
        } finally {
            if(client != null){
                //关闭
                client.shutdown();
            }
        }

        String fileUrl = config.getAliyunDomain() + "/" + path;
        System.out.println("config.getAliyunDomain()" + config.getAliyunDomain());
        return path;
        //return fileUrl;
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return upload(data, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return upload(inputStream, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public boolean deleteFileByFilePath(String filePath) {
       try {
           System.out.println("你好你好");
           System.out.println("名称："+config.getAliyunBucketName());
           /*boolean exist = client.doesObjectExist(config.getAliyunBucketName(),filePath);
           System.out.println("布尔值"+exist);
           if (exist) {
               return false;
           }*/
           client.deleteObject(config.getAliyunBucketName(), filePath);// https://xunxuebao.oss-cn-shanghai.aliyuncs.com     /20200601/6dfa30234axxk33.jpg
       }
       catch (Exception e){
           throw new RRException("删除文件失败", e);
       } finally {
           if(client != null){
               //关闭
               client.shutdown();
           }
       }


        return true;
    }
}
