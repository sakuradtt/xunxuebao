package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.Course;
import io.renren.modules.sys.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/course")
@Api(tags = "课程管理")
public class CourseController {

    @Resource
    private CourseService courseService;

    @GetMapping("/page")
    @ApiOperation("根据条件分页查询课程信息")
    public R list(@RequestParam Map<String, Object> paramMap) {
        String cname = String.valueOf(paramMap.get("cname"));
        Integer page = Integer.valueOf(String.valueOf(paramMap.get("page")));
        Integer limit = Integer.valueOf(String.valueOf(paramMap.get("limit")));
        Integer status = Integer.valueOf(String.valueOf(paramMap.get("status")));
        PageUtils courseByPage = courseService.findCourseByPage(cname, status, page, limit);
        return R.ok().put("page", courseByPage);
    }

    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation("批量删除")
    public R deleteBatch(@PathVariable("ids[]") ArrayList<Integer> ids) {
        boolean result = courseService.deleteSchoolBatch(ids);
        if (result) {
            return R.ok("批量删除成功");
        } else {
            return R.error("批量删除失败");
        }
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除课程")
    public R deleteCourse(@PathVariable("id") int id) {
        System.out.println("id"+id);
        boolean result = courseService.deleteCourseById(id);
        if (result) {
            return R.ok("删除成功");
        } else {
            return R.error("删除失败");
        }
    }

    @GetMapping("/select")
    public R selectAll() {
        List<Course> courses = courseService.selectAllCourse();
        return R.ok().put("data", courses);
    }

    @GetMapping("/select/{id}")
    public R selectOne(@PathVariable("id") int id) {
        Course courses = courseService.selectOneCourse(id);
        return R.ok().put("data", courses);
    }

    @ApiOperation("添加课程")
    @PostMapping("/save")
    public R saveCourse(@RequestBody Course course) {
        int res = courseService.saveCourse(course);
        if (res>0) {
            return R.ok("添加课程成功");
        } else {
            return R.error("添加课程失败");
        }
    }

    @ApiOperation("修改课程")
    @PostMapping("/update")
    public R updateCourse(@RequestBody Course course) {
        boolean b = courseService.courseUpdate(course);
        if (b) {
            return R.ok("修改课程成功");
        } else {
            return R.error("修改课程失败");
        }
    }

    @PostMapping("/updateCourse/{ids[]}")
    @ApiOperation("批量修改校区")
    public R updateBeatchSchool(@PathVariable("ids[]")  ArrayList<Integer> ids){
        boolean b = courseService.updateBath(ids);
        if (b){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }
}

