package io.renren.modules.sys.controller;


import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.Student;
import io.renren.modules.sys.service.StudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping(value = "/sys/student")
public class StudentController {

    @Resource
    private StudentService studentService;

    @GetMapping("/search")
    public R list(Map<String,Object> paramMap){
        PageUtils page = studentService.findByPage(paramMap);
        return R.ok().put("page",page);
    }

    @PostMapping("/save")
    public R add(@RequestBody Student student){
        boolean result = studentService.save(student);
        return R.ok(result ? "保存学员信息成功":"保存学员信息失败");
    }

    @PostMapping("/modify")
    public R update(@RequestBody Student student){
        boolean result = studentService.updateById(student);
        return R.ok(result ? "修改学员信息成功":"修改学员信息失败");
    }

    @PostMapping("/delete/{sid}")
    public R logicDel(@PathVariable(value = "sid") int sid){
        boolean result = studentService.removeById(sid);
        return R.ok(result ? "删除学员信息成功":"删除学员信息失败");
    }
}
