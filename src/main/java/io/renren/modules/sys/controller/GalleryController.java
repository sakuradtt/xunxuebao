package io.renren.modules.sys.controller;

import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.oss.cloud.OSSFactory;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.entity.Gallery;
import io.renren.modules.sys.service.GalleryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/gallery")
@Api(tags = "相册管理接口")
public class GalleryController {

    @Resource
    private GalleryService galleryService;

    @GetMapping("/getList")
    @ApiOperation("根据机构名称分页显示机构列表")
    public R getGalleryList(@RequestParam Map<String,Object> params){
        String title = String.valueOf(params.get("title"));
        int page = Integer.valueOf(String.valueOf(params.get("page")));
        int limit = Integer.valueOf(String.valueOf(params.get("limit")));
        PageUtils pageUtils = galleryService.getGalleryByPage(page,limit,title);
        return R.ok().put("data",pageUtils);
    }


    @GetMapping("/getById/{id}")
    @ApiOperation("根据编号查询机构")
    public R getGalleryList(@PathVariable("id") int id){
        Gallery gallery = galleryService.getGalleryById(id);
        //System.out.println(gallery.toString());
        return R.ok().put("data",gallery);
    }


    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据机构编号删除机构")
    public R deleteGalleryById(@PathVariable("id") int id){
        boolean result = galleryService.deleteOne(id);
        if (result){
            return R.ok().put("msg","删除行业分类成功");
        }else {
            return R.error("删除行业分类失败");
        }
    }


    @PostMapping("/save")
    @ApiOperation("新增机构")
    public R saveGallery(@RequestBody Gallery gallery){
        boolean result = galleryService.addGallery(gallery);
        if (result){
            return R.ok().put("msg","添加行业分类成功");
        }else {
            return R.error("添加行业分类失败");
        }
    }

    @PostMapping("/modify")
    @ApiOperation("修改机构")
    public R updateGallery(@RequestBody Gallery gallery){
        boolean result = galleryService.updateGallery(gallery);
        if (result){
            return R.ok().put("msg","修改机构信息成功");
        }else {
            return R.error("修改机构信息失败");
        }
    }

    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation(value = "批量删除机构")
    public R deleteBatchClassify(@PathVariable("ids[]") ArrayList<Integer> idList){
        boolean result = galleryService.deleteBatch(idList);
        if (result){
            return R.ok().put("msg","批量删除机构信息成功");
        }else {
            return R.error("批量删除机构信息失败");
        }
    }

}
