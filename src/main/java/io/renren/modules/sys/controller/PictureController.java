package io.renren.modules.sys.controller;

import io.renren.common.exception.RRException;
import io.renren.common.utils.R;
import io.renren.modules.oss.cloud.OSSFactory;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.entity.Picture;
import io.renren.modules.sys.service.PictureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/sys/picture")
@Api(tags = "照片管理接口")
public class PictureController {

    @Resource
    private PictureService pictureService;

    @GetMapping("/getListByGid/{galleryId}")
    @ApiOperation("根据编号查询机构")
    public R getPictureList(@PathVariable("galleryId") int galleryId){
        List<Picture> pictureList = pictureService.getListByGalleryId(galleryId);
        //System.out.println(company.toString());
        return R.ok().put("data",pictureList);
    }


    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据机构编号删除机构")
    public R deleteCompanyById(@PathVariable("id") int id){
        boolean result = pictureService.deleteOne(id);
        if (result){
            return R.ok().put("msg","删除行业分类成功");
        }else {
            return R.error("删除行业分类失败");
        }
    }


    @PostMapping("/save")
    @ApiOperation("新增机构")
    public R saveCompany(@RequestBody Picture picture){
        boolean result = pictureService.addPicture(picture);
        if (result){
            return R.ok().put("msg","添加行业分类成功");
        }else {
            return R.error("添加行业分类失败");
        }
    }

    @PostMapping("/modify")
    @ApiOperation("修改机构")
    public R updateCompany(@RequestBody Picture picture){
        boolean result = pictureService.updatePicture(picture);
        if (result){
            return R.ok().put("msg","修改机构信息成功");
        }else {
            return R.error("修改机构信息失败");
        }
    }

    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation(value = "批量删除机构")
    public R deleteBatchClassify(@PathVariable("ids[]") ArrayList<Integer> idList){
        boolean result = pictureService.deleteBatch(idList);
        if (result){
            return R.ok().put("msg","批量删除机构信息成功");
        }else {
            return R.error("批量删除机构信息失败");
        }
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        //上传文件
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix);

        url = "https://xun-xunbao.oss-cn-shanghai.aliyuncs.com/" + url;
        return R.ok().put("url",url);
    }

}
