package io.renren.modules.sys.controller;

import io.renren.common.utils.R;
import io.renren.modules.sys.entity.Area;
import io.renren.modules.sys.service.AreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/sys/area")
@Api(tags = "区域管理接口")
public class AreaController extends AbstractController {

    @Resource
    private AreaService areaService;

    @GetMapping("/getTree/{pid}")
    @ApiOperation( value = "获取地区列表")
    public R getAreaTree(@PathVariable(value = "pid") int pid){
        List<Area> areaList = areaService.getAreaByPid(pid);
        return R.ok().put("data",areaList);
    }
}
