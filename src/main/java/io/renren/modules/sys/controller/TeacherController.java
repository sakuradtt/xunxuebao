package io.renren.modules.sys.controller;


import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.oss.cloud.OSSFactory;
import io.renren.modules.sys.entity.Teacher;
import io.renren.modules.sys.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/sys/teacher")
@Api(tags = "教师管理接口")
public class TeacherController {

    @Resource
    private TeacherService teacherService;

    @GetMapping("/list")
    @ApiOperation("根据教师名称分页显示机构列表")
    public R getTeacherByPage(@RequestParam Map<String,Object> params){
        String teaName = String.valueOf(params.get("teaName"));
        int page = Integer.valueOf(String.valueOf(params.get("page")));
        int limit = Integer.valueOf(String.valueOf(params.get("limit")));
        int status = Integer.valueOf(String.valueOf(params.get("status")));
        PageUtils pageUtils = teacherService.getTeacherByPage(page,limit,teaName,status);
        return R.ok().put("data",pageUtils);
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据编号查询教师")
    public R getTeacher(@PathVariable("id") int id){
        Teacher teacher = teacherService.getTeacherById(id);
        return R.ok().put("data",teacher);
    }


    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据教师编号删除教师")
    public R deleteTeacherById(@PathVariable("id") int id){
        boolean result = teacherService.deleteOne(id);
        if (result){
            return R.ok().put("msg","删除行业分类成功");
        }else {
            return R.error("删除行业分类失败");
        }
    }


    @PostMapping("/save")
    @ApiOperation("新增机构")
    public R saveTeacher(@RequestBody Teacher teacher){
        boolean result = teacherService.addTeacher(teacher);
        if (result){
            return R.ok().put("msg","添加行业分类成功");
        }else {
            return R.error("添加行业分类失败");
        }
    }

    @PostMapping("/modify")
    @ApiOperation("修改机构")
    public R updateTeacher(@RequestBody Teacher teacher){
        System.out.println(teacher.toString());
        boolean result = teacherService.updateTeacher(teacher);
        if (result){
            return R.ok().put("msg","修改机构信息成功");
        }else {
            return R.error("修改机构信息失败");
        }
    }

    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation(value = "批量删除机构")
    public R deleteBatchClassify(@PathVariable("ids[]") ArrayList<Integer> idList){
        boolean result = teacherService.deleteBatch(idList);
        if (result){
            return R.ok().put("msg","批量删除机构信息成功");
        }else {
            return R.error("批量删除机构信息失败");
        }
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        //上传文件
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix);

        url = "https://xun-xunbao.oss-cn-shanghai.aliyuncs.com/" + url;
        System.out.println(url);
        return url;
    }

}
