package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.School;
import io.renren.modules.sys.service.SchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/school")
@Api(tags = "校区管理")
public class SchoolController {

    @Resource
    private SchoolService schoolService;


    @GetMapping("/index")
    @ApiOperation("根据条件分页查询校区信息")
    public R list(@RequestParam Map<String, Object> paramMap){
        String companyName = String.valueOf(paramMap.get("companyName"));
        Integer page = Integer.valueOf(String.valueOf(paramMap.get("page")));
        Integer limit = Integer.valueOf(String.valueOf(paramMap.get("limit")));
        String name = String.valueOf(paramMap.get("name"));
        String principal = String.valueOf(paramMap.get("principal"));
        String phone = String.valueOf(paramMap.get("phone"));
        String address = String.valueOf(paramMap.get("address"));
        Integer status = Integer.valueOf(String.valueOf(paramMap.get("status")));
        PageUtils companyByPage = schoolService.findCompanyByPage(companyName,page, limit, name, principal, phone, address, status);
        return R.ok().put("page", companyByPage);
    }


    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation("批量删除")
    public R deleteBatch(@PathVariable("ids[]") ArrayList<Integer> ids){
        boolean b = schoolService.deleteSchoolBatch(ids);
        if (b){
            return R.ok("批量删除成功");
        }else {
            return R.error("批量删除失败");
        }
    }
    @PostMapping("/BeatchUpdate/{ids[]}")
    @ApiOperation("批量修改校区")
    public R updateBeatchSchool(@PathVariable("ids[]")  ArrayList<Integer> ids){
        boolean b = schoolService.updateBath(ids);
        if (b){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }


    @PostMapping("/delete/{id}")
    @ApiOperation("删除一个")
    public R deleteBatch(@PathVariable("id") int id){
        boolean b = schoolService.removeById(id);
        if (b){
            return R.ok("删除成功");
        }else {
            return R.error("删除失败");
        }
    }


    @ApiOperation("查找校区")
    @GetMapping("/select")
    public R selectSchool(){
        List<School> allSchool = schoolService.findAllSchool();
        return R.ok().put("data",allSchool);
    }

    @PostMapping("/save")
    @ApiOperation("添加校区")
    public R saveSchool(@RequestBody School school){
        boolean b = schoolService.saveSchool(school);
        if (b){
            return R.ok("添加成功");
        }else {
            return R.error("添加失败");
        }
    }

    @GetMapping("/select/{id}")
    @ApiOperation("查询一个校区")
    public R selectSchool(@PathVariable("id")int id){
        School school = schoolService.selectSchoolOne(id);
        return R.ok("查找成功").put("school",school);
    }

    @PostMapping("/update")
    @ApiOperation("修改校区")
    public R updateSchool(@RequestBody School school){
        boolean b = schoolService.updateSchool(school);
        if (b){
            return R.ok("修改成功");
        }else {
            return R.error("修改失败");
        }
    }


}

