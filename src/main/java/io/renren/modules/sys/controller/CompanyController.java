package io.renren.modules.sys.controller;

import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.oss.cloud.OSSFactory;
import io.renren.modules.oss.entity.SysOssEntity;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/sys/company")
@Api(tags = "机构管理接口")
public class CompanyController {

    @Resource
    private CompanyService companyService;


    @GetMapping("/list")
    @ApiOperation("分页显示机构列表")
    public R companyList(@RequestParam Map<String,Object> params){
        PageUtils page = companyService.queryPageCompany(params);
        return R.ok().put("data",page);
    }

    @GetMapping("/getList")
    @ApiOperation("根据机构名称分页显示机构列表")
    public R getCompanyList(@RequestParam Map<String,Object> params){
        String comName = String.valueOf(params.get("comName"));
        int page = Integer.valueOf(String.valueOf(params.get("page")));
        int limit = Integer.valueOf(String.valueOf(params.get("limit")));
        int status = Integer.valueOf(String.valueOf(params.get("status")));
        PageUtils pageUtils = companyService.getCompanyByPage(page,limit,comName,status);
        return R.ok().put("data",pageUtils);
    }

    @GetMapping("/getAll")
    @ApiOperation("获得机构列表")
    public R getCompanyList(){
        List<Company> companyList = companyService.getAll();
        return R.ok().put("data",companyList);
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据编号查询机构")
    public R getCompanyList(@PathVariable("id") int id){
        Company company = companyService.getCompanyById(id);
        System.out.println(company.toString());
        return R.ok().put("data",company);
    }


    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据机构编号删除机构")
    public R deleteCompanyById(@PathVariable("id") int id){
        boolean result = companyService.deleteOne(id);
        if (result){
            return R.ok().put("msg","删除行业分类成功");
        }else {
            return R.error("删除行业分类失败");
        }
    }


    @PostMapping("/save")
    @ApiOperation("新增机构")
    public R saveCompany(@RequestBody Company company){
        System.out.println(company.getMasterImgs());
        boolean result = companyService.addCompany(company);
        if (result){
            return R.ok().put("msg","添加行业分类成功");
        }else {
            return R.error("添加行业分类失败");
        }
    }

    @PostMapping("/modify")
    @ApiOperation("修改机构")
    public R updateCompany(@RequestBody Company company){
        boolean result = companyService.updateCompany(company);
        if (result){
            return R.ok().put("msg","修改机构信息成功");
        }else {
            return R.error("修改机构信息失败");
        }
    }

    @DeleteMapping("/deleteBatch/{ids[]}")
    @ApiOperation(value = "批量删除机构")
    public R deleteBatchClassify(@PathVariable("ids[]") ArrayList<Integer> idList){
        boolean result = companyService.deleteBatch(idList);
        if (result){
            return R.ok().put("msg","批量删除机构信息成功");
        }else {
            return R.error("批量删除机构信息失败");
        }
    }

    @PostMapping("/updateStatus/{ids[]}")
    @ApiOperation(value = "批量修改机构状态")
    public R updateStatusList(@PathVariable("ids[]") List<Integer> idList){
        boolean result = companyService.updateStatusList(idList);
        if (result){
            return R.ok().put("msg","批量审核机构状态成功");
        }else {
            return R.error("批量审核机构状态失败");
        }
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }

        //上传文件
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String url = OSSFactory.build().uploadSuffix(file.getBytes(), suffix);

        url = "https://xun-xunbao.oss-cn-shanghai.aliyuncs.com/" + url;
        return R.ok().put("url",url);
    }

}
