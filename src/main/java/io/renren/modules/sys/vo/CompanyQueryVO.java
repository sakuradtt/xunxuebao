package io.renren.modules.sys.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CompanyQueryVO implements Serializable {
    private String comName;

}
