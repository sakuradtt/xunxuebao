package io.renren.modules.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("分类实体类")
public class ClassifyVo implements Serializable {
    @ApiModelProperty("分类编号")
    private Integer id;

    @ApiModelProperty("分类名称")
    private String industryName;

    @ApiModelProperty("父分类编号")
    private Integer parentId;

    @ApiModelProperty("分类图标")
    private String iconPath;

    @ApiModelProperty("分类级别")
    private Integer depth;

    @ApiModelProperty("分类排序")
    private Integer sort;

    @ApiModelProperty("是否热门")
    private Integer isHot;

}
