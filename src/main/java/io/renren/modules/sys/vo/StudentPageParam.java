package io.renren.modules.sys.vo;

import lombok.Data;

@Data
public class StudentPageParam {
    private String phone;
    private String name;
    private int page;
    private int limit;
}
