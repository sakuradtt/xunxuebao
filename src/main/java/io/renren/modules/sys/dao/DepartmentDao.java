package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.service.impl.Department;



public interface DepartmentDao extends BaseMapper<Department> {


}
