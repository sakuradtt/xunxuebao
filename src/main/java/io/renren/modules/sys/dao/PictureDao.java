package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Gallery;
import io.renren.modules.sys.entity.Picture;

import java.util.List;

public interface PictureDao extends BaseMapper<Picture> {
    int getTotal(int GalleryId);

    List<Picture> getByGalleryId(int GalleryId);
}
