package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Gallery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GalleryDao extends BaseMapper<Gallery> {
    Gallery findGalleryByName(String title);

    List<Gallery> getGalleryByPage(@Param(value = "page") int page,
                                   @Param(value = "limit") int limit,
                                   @Param(value = "title") String title);

    int getTotal(String title);

    Gallery getGalleryById(int id);
}
