package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Classify;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClassifyDao extends BaseMapper<Classify> {

    List<Classify> findClassifyByPid(int pid);

    Classify findClassifyByClassName(String className);

    Classify findClassifyById(int id);

    List<Classify> getClassifyByPage(@Param(value = "industryName") String industryName,
                                     @Param(value = "depth") int depth);
}
