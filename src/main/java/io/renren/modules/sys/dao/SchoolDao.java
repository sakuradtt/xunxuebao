package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.School;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SchoolDao extends BaseMapper<School> {
    List<School> getSchoolByPage(
            @Param(value = "companyId") int companyId,
            @Param(value = "name") String name,
            @Param(value = "principal") String principal,
            @Param(value = "phone") String phone,
            @Param(value = "address") String address,
            @Param(value = "status") int status,
            @Param(value = "page") int page,
            @Param("limit") int limit);

    Integer getSchoolTotalCount(
            @Param(value = "name") String name,
            @Param(value = "principal") String principal,
            @Param(value = "phone") String phone,
            @Param(value = "address") String address,
            @Param(value = "status") Integer status);

    List<School> selectAllSchool();

    boolean addSchool(School school);

    void batchDelete(List<Integer> list);


    boolean updateSchool(School school);

    School selectSchool(int id);

    boolean updataSchoolBath(@Param(value = "ids")List<Integer> ids);
}
