package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Area;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AreaDao extends BaseMapper<Area> {
    List<Area> getAllArea();

    List<Area> getAreaByPid(@Param("pid") int pid);

    Area findAreaById(@Param("id") int id);
}
