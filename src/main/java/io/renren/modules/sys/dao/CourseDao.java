package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CourseDao extends BaseMapper<Course> {

    List<Course> getCourseByPage(
            @Param(value = "cname") String cname,
            @Param(value = "status") int status,
            @Param(value = "page") int page,
            @Param("limit") int limit);


    int getCourseTotalCount( @Param(value = "cname") String cName,
                             @Param(value = "status") int status);



    boolean insertCourse(Course course);

    boolean updateCourse(Course course);

    boolean updataCourseBath(@Param(value = "ids")List<Integer> ids);

    Course getCourseById(int id);

}
