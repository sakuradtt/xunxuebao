package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Student;

import java.util.List;

public interface StudentDao extends BaseMapper<Student> {

}
