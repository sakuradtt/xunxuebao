package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Company;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyDao extends BaseMapper<Company> {

    Company findCompanyByName(String name);

    List<Company> getCompanyByPage(@Param(value = "page") int page,
                                   @Param(value = "limit") int limit,
                                   @Param(value = "name") String name,
                                   @Param(value = "status") int status);

    int getTotal(@Param(value = "name") String name,
                 @Param(value = "status") int status);

    Company getCompanyById(int id);

    List<Company> getAll();

    int updateStatusList(@Param(value = "idList") List<Integer> idList);
}
