package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeacherDao extends BaseMapper<Teacher> {

    Teacher findTeacherByName(String name);

    List<Teacher> getTeacherByPage(@Param(value = "page") int page,
                                   @Param(value = "limit") int limit,
                                   @Param(value = "tName") String tName,
                                   @Param(value = "status") int status);

    int getCount(@Param(value = "tName") String tName,
                 @Param(value = "status") int status);

    Teacher getTeacherById(int id);
}
