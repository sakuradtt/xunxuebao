package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.ClassifyDao;
import io.renren.modules.sys.entity.Classify;
import io.renren.modules.sys.service.ClassifyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class ClassifyServiceImpl extends ServiceImpl<ClassifyDao, Classify> implements ClassifyService {

    @Resource
    private ClassifyDao classifyDao;


    @Override
    public List<Classify> queryClassifyAll(String industryName, int depth) {
        List<Classify> classifys = classifyDao.getClassifyByPage(industryName,depth);
        System.out.println(classifys.toString());
        //2、组装成父子的树型目录结构
        //2.1、先找到所有的一级行业分类
        List<Classify> parentClassifys = classifys.stream()
                .filter(clazz -> clazz.getParentId()==0)
                .map(clazz ->
                {clazz.setChildren(getChildrens(clazz,classifys));
                    return clazz;
                }).sorted((cat1,cat2)->{
                    return (cat1.getSort()==null?0:cat1.getSort())- (cat2.getSort() == null ? 0 : cat2.getSort());
                }).collect(Collectors.toList());
        return parentClassifys;
    }

    @Override
    public List<Classify> getAllParentClassify() {
        List<Classify> classifyList = classifyDao.selectList(null);
        //2、组装成父子的树型目录结构
        //2.1、先找到所有的一级行业分类
        List<Classify> allParentClassify = classifyList.stream()
                .filter(clazz -> clazz.getParentId()==0)
                .map(clazz ->
                        {clazz.setChildren(getChildrens(clazz,classifyList));
                        return clazz;
                        }).sorted((cat1,cat2)->{
                            return (cat1.getSort()==null?0:cat1.getSort())- (cat2.getSort() == null ? 0 : cat2.getSort());
                }).collect(Collectors.toList());
        return allParentClassify;
    }

    @Override
    public boolean addClassify(Classify entity) {
        Classify classify = classifyDao.findClassifyByClassName(entity.getIndustryName());
        if (classify!= null){
            return false;
        }else {
            return classifyDao.insert(entity)>0;
        }
    }

    @Override
    public boolean updateClassify(Classify entity) {
        QueryWrapper<Classify> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("id",entity.getId());
        queryWrapper.eq("industry_name",entity.getIndustryName());
        Classify classify = classifyDao.selectOne(queryWrapper);
        if (classify!=null){
            return false;
        }
        return classifyDao.updateById(entity)>0;
    }

    @Override
    public boolean deleteBatch(List<Integer> idList) {
        return classifyDao.deleteBatchIds(idList)>0;
    }

    @Override
    public boolean deleteOne(Integer id) {
        return classifyDao.deleteById(id)>0;
    }

    @Override
    public List<Classify> getClassifyByPid(Integer pid) {
        return classifyDao.findClassifyByPid(pid);
    }

    @Override
    public boolean updateIconPath(Integer id, String iconPath) {
        Classify classify = classifyDao.selectById(id);
        classify.setIconPath(iconPath);

        return classifyDao.updateById(classify)>0;
    }

    @Override
    public boolean updateHotRecommend(Integer id, int isHot) {
        Classify classify = classifyDao.selectById(id);
        classify.setIsHot(isHot);
        return classifyDao.updateById(classify)>0;
    }

    @Override
    public Classify getClassifyById(Integer id) {
        return classifyDao.selectById(id);
    }


    //递归查找所有行业分类的子行业类
    private List<Classify> getChildrens(Classify root,List<Classify> classifies){
        List<Classify> children = classifies.stream()
                .filter(classify -> {
                    //找到某个父分类下所有子分类
                    return classify.getParentId().equals(root.getId());
                })
                .map(classify -> {
                    classify.setChildren(getChildrens(classify, classifies));
                    return classify;
                }) //行业分类的排序
                .sorted((class1, class2) -> {
                    return (class1.getSort() == null ? 0 : class1.getSort()) - (class2.getSort() == null ? 0 : class2.getSort());
                })
                .collect(Collectors.toList());

        return children;
    }


}
