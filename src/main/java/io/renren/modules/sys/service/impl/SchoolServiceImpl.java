package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.dao.CompanyDao;
import io.renren.modules.sys.dao.SchoolDao;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.entity.School;
import io.renren.modules.sys.service.SchoolService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class SchoolServiceImpl extends ServiceImpl<SchoolDao, School> implements SchoolService {

    @Resource
    private SchoolDao schoolDao;
    @Resource
    private CompanyDao companyDao;
    @Override
    public List<School> findAllSchool() {
        List<School> schoolList = schoolDao.selectAllSchool();
        return schoolList;
    }
    @Override
    public PageUtils findCompanyByPage(String companyName,int page, int limit, String name, String principal, String phone, String address, int status) {
        Company company = companyDao.findCompanyByName(companyName);
        List<School> schoolByPage = new ArrayList<>();
        Integer totalCount = 0;
        if (company!=null) {
            schoolByPage = schoolDao.getSchoolByPage(company.getId(), name, principal, phone, address, status, page, limit);
            totalCount = schoolDao.getSchoolTotalCount(name, principal, phone, address, status);

        }else {
            schoolByPage = schoolDao.getSchoolByPage(-1, name, principal, phone, address, status, page, limit);
            totalCount = schoolDao.getSchoolTotalCount(name, principal, phone, address, status);
        }
        PageUtils pageUtils = new PageUtils(schoolByPage, totalCount, page, limit);
        return pageUtils;
    }

    @Override
    public boolean deleteSchoolBatch(List<Integer> ids) {
        return schoolDao.deleteBatchIds(ids)>0;
    }

    @Override
    public boolean updateBath(List<Integer> ids) {
        return schoolDao.updataSchoolBath(ids);
    }

    @Override
    public boolean saveSchool(School school) {
        return schoolDao.addSchool(school);
    }

    @Override
    public School selectSchoolOne(int id) {
        return schoolDao.selectSchool(id);
    }

    @Override
    public boolean updateSchool(School school) {
        return schoolDao.updateSchool(school);
    }



}

