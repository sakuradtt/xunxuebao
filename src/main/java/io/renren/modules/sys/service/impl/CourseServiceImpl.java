package io.renren.modules.sys.service.impl;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.dao.CourseDao;
import io.renren.modules.sys.entity.Course;
import io.renren.modules.sys.service.CourseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class CourseServiceImpl extends ServiceImpl<CourseDao, Course> implements CourseService {

    @Resource
    private CourseDao courseDao;


    @Override
    public PageUtils findCourseByPage(String cname,int status, int page, int limit) {
        List<Course> courseByPage = courseDao.getCourseByPage(cname,status, page, limit);
        int courseTotalCount = courseDao.getCourseTotalCount(cname,status);
        PageUtils pageUtils = new PageUtils(courseByPage, courseTotalCount, page, limit);

        return pageUtils;
    }

    @Override
    public boolean deleteSchoolBatch(List<Integer> ids) {
        return courseDao.deleteBatchIds(ids)>0;
    }

    @Override
    public List<Course> selectAllCourse() {
        QueryWrapper queryWrapper=new QueryWrapper();
        List list = courseDao.selectList(queryWrapper);
        return list;
    }

    @Override
    public int saveCourse(Course course) {
        return courseDao.insert(course);

    }

    @Override
    public boolean updateBath(List<Integer> ids) {
        return courseDao.updataCourseBath(ids);
    }

    @Override
    public boolean courseUpdate(Course course) {
        return  courseDao.updateCourse(course);
    }

    @Override
    public Course selectOneCourse(int id) {
        return courseDao.getCourseById(id);
    }



    @Override
    public boolean deleteCourseById(int id) {
        return courseDao.deleteById(id)>0;

    }


}
