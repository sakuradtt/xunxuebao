package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.CompanyDao;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.service.CompanyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class CompanyServiceImpl extends ServiceImpl<CompanyDao, Company> implements CompanyService {

    @Resource
    private CompanyDao companyDao;

    @Override
    public PageUtils queryPageCompany(Map<String, Object> params) {
        String comName = String.valueOf(params.get("comName"));
        IPage<Company> page = this.page(
                new Query<Company>().getPage(params),
                new QueryWrapper<Company>().like(StringUtils.isNotBlank(comName),"name",comName)
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils getCompanyByPage(int page, int limit, String name,int status) {
        int total = companyDao.getTotal(name,status);
        List<Company> companyList = companyDao.getCompanyByPage(page,limit,name,status);
        PageUtils pageUtils = new PageUtils(companyList,total,page,limit);
        return pageUtils;
    }

    @Override
    public boolean addCompany(Company company) {
        Company companyByName = companyDao.findCompanyByName(company.getName());
        if (companyByName!= null){
            return false;
        }else {
            return companyDao.insert(company)>0;
        }
    }

    @Override
    public boolean updateCompany(Company company) {
        return companyDao.updateById(company)>0;
    }

    @Override
    public boolean deleteBatch(List<Integer> idList) {
        return companyDao.deleteBatchIds(idList)>0;
    }

    @Override
    public boolean deleteOne(Integer id) {
        return companyDao.deleteById(id)>0;
    }

    @Override
    public Company getCompanyById(int id) {
        return companyDao.getCompanyById(id);
    }

    @Override
    public List<Company> getAll() {
        return companyDao.getAll();
    }

    @Override
    public boolean updateStatusList(List<Integer> idList) {
        return companyDao.updateStatusList(idList)>0;
    }
}
