package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.Company;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CompanyService extends IService<Company> {

    PageUtils queryPageCompany(Map<String,Object> params);

    PageUtils getCompanyByPage(int page,int limit,String name,int status);

    boolean addCompany(Company company);

    boolean updateCompany(Company company);

    boolean deleteBatch(List<Integer> idList);

    boolean deleteOne(Integer id);

    Company getCompanyById(int id);

    List<Company> getAll();

    boolean updateStatusList(List<Integer> idList);
}
