package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@TableName("classify")
@Data
public class Department implements Serializable {

    private Integer id;
    private String name;
    private Integer parentId;

    @TableField(exist = false)
    private List<Department> children;
}
