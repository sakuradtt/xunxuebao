package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.dao.GalleryDao;
import io.renren.modules.sys.entity.Gallery;
import io.renren.modules.sys.service.GalleryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class GalleryServiceImpl extends ServiceImpl<GalleryDao, Gallery> implements GalleryService {

    @Resource
    private GalleryDao galleryDao;

    @Override
    public PageUtils getGalleryByPage(int page, int limit, String title) {
        int total = galleryDao.getTotal(title);
        List<Gallery> galleryList = galleryDao.getGalleryByPage(page,limit,title);
        PageUtils pageUtils = new PageUtils(galleryList,total,limit,page);
        return pageUtils;
    }

    @Override
    public boolean addGallery(Gallery gallery) {
        return galleryDao.insert(gallery)>0;
    }

    @Override
    public boolean updateGallery(Gallery gallery) {
        return galleryDao.updateById(gallery)>0;
    }

    @Override
    public boolean deleteBatch(List<Integer> idList) {
        return galleryDao.deleteBatchIds(idList)>0;
    }

    @Override
    public boolean deleteOne(Integer id) {
        return galleryDao.deleteById(id)>0;
    }

    @Override
    public Gallery getGalleryById(int id) {
        return galleryDao.getGalleryById(id);
    }
}
