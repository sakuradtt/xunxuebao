package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.dao.TeacherDao;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.entity.Teacher;
import io.renren.modules.sys.service.TeacherService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class TeacherServiceImpl extends ServiceImpl<TeacherDao, Teacher> implements TeacherService {

    @Resource
    private TeacherDao teacherDao;

    @Override
    public PageUtils getTeacherByPage(int page, int limit, String tName, int status) {
        int total = teacherDao.getCount(tName,status);
        List<Teacher> teacherList = teacherDao.getTeacherByPage(page,limit,tName,status);
        PageUtils pageUtils = new PageUtils(teacherList,total,page,limit);
        return pageUtils;
    }

    @Override
    public boolean addTeacher(Teacher teacher) {
        Teacher teacherByName = teacherDao.findTeacherByName(teacher.getTName());
        if (teacherByName!= null){
            return false;
        }else {
            return teacherDao.insert(teacher)>0;
        }
    }

    @Override
    public boolean updateTeacher(Teacher teacher) {
        return teacherDao.updateById(teacher)>0;
    }

    @Override
    public boolean deleteBatch(List<Integer> idList) {
        return teacherDao.deleteBatchIds(idList)>0;
    }

    @Override
    public boolean deleteOne(Integer id) {
        return teacherDao.deleteById(id)>0;
    }

    @Override
    public Teacher getTeacherById(int id) {
        return teacherDao.getTeacherById(id);
    }
}
