package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.StudentDao;
import io.renren.modules.sys.entity.Student;
import io.renren.modules.sys.service.StudentService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {

    @Override
    public PageUtils findByPage(Map<String,Object> param){
        String name = String.valueOf(param.get("name"));
        String phone = String.valueOf(param.get("phone"));
        IPage<Student> page = this.page(
                new Query<Student>().getPage(param),
                new QueryWrapper<Student>().like(StringUtils.isNotBlank(name),"name",name)
                        .like(StringUtils.isNotBlank(phone),"phone",phone)
        );
        return new PageUtils(page);
    }

}
