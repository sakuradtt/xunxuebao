package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.AreaDao;
import io.renren.modules.sys.entity.Area;
import io.renren.modules.sys.service.AreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class AreaServiceImpl extends ServiceImpl<AreaDao,Area> implements AreaService {

    @Resource
    private AreaDao areaDao;

    @Override
    public List<Area> getAllAreaTree() {
        /*List<Area> areaList = areaDao.getAllArea();
        for (Area area:areaList) {
            area.setChildren(areaTreeList(areaList,area));
        }
        return areaList;*/
        return null;
    }

    @Override
    public List<Area> getAreaByPid(int pid) {
        List<Area> areaList = areaDao.getAreaByPid(pid);
        for (Area area:areaList) {
            List<Area> tempList = areaDao.getAreaByPid(area.getId());
            area.setHasChildren(tempList.size()>0?true:false);
        }
        return areaList;
    }



   /* private List<Area> areaTreeList(List<Area> list,Area root){
        List<Area> areaList = new ArrayList<>();
        for (Area area:list) {
            if (area.getParentId().intValue()==root.getId().intValue()){
                List<Area> areaTempList = areaTreeList(list,area);
                area.setChildren(areaTempList);
                areaList.add(area);
            }
        }
        return areaList;
    }*/

}
