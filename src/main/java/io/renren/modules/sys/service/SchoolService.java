package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.School;


import java.util.List;
import java.util.Map;

public interface SchoolService extends IService<School> {

    List<School> findAllSchool();
    /* PageUtils queryPage(Map<String, Object> params);*/


    PageUtils findCompanyByPage(String companyName,int page, int limit, String name, String principal, String phone, String address, int status);

    boolean deleteSchoolBatch(List<Integer> ids );

    boolean updateBath(List<Integer> ids);

    boolean saveSchool(School school);

    School selectSchoolOne(int id);

    boolean updateSchool(School school);


}

