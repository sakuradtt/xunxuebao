package io.renren.modules.sys.service;

import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.Course;

import java.util.List;

public interface CourseService {

    PageUtils findCourseByPage(String cname, int status, int page, int limit);

    boolean deleteSchoolBatch(List<Integer> ids );

    boolean deleteCourseById(int id);

    List<Course> selectAllCourse();


    Course selectOneCourse(int id);

    int saveCourse(Course course);

    boolean courseUpdate(Course course);

    boolean updateBath(List<Integer> ids);

}
