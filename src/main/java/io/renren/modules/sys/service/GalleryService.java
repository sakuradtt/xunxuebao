package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.Company;
import io.renren.modules.sys.entity.Gallery;

import java.util.List;

public interface GalleryService extends IService<Gallery> {

    PageUtils getGalleryByPage(int page, int limit, String title);

    boolean addGallery(Gallery gallery);

    boolean updateGallery(Gallery gallery);

    boolean deleteBatch(List<Integer> idList);

    boolean deleteOne(Integer id);

    Gallery getGalleryById(int id);
}
