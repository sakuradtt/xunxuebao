package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.PictureDao;
import io.renren.modules.sys.entity.Picture;
import io.renren.modules.sys.service.PictureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class PictureServiceImpl extends ServiceImpl<PictureDao, Picture> implements PictureService {

    @Resource
    private PictureDao pictureDao;

    @Override
    public boolean addPicture(Picture picture) {
        return pictureDao.insert(picture)>0;
    }

    @Override
    public boolean updatePicture(Picture picture) {
        return pictureDao.updateById(picture)>0;
    }

    @Override
    public boolean deleteBatch(List<Integer> idList) {
        return pictureDao.deleteBatchIds(idList)>0;
    }

    @Override
    public boolean deleteOne(Integer id) {
        return pictureDao.deleteById(id)>0;
    }

    @Override
    public List<Picture> getListByGalleryId(int galleryId) {
        return pictureDao.getByGalleryId(galleryId);
    }

}
