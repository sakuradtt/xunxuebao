package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.Teacher;

import java.util.List;

public interface TeacherService extends IService<Teacher> {

    PageUtils getTeacherByPage(int page, int limit, String name, int status);

    boolean addTeacher(Teacher teacher);

    boolean updateTeacher(Teacher teacher);

    boolean deleteBatch(List<Integer> idList);

    boolean deleteOne(Integer id);

    Teacher getTeacherById(int id);
}
