package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.entity.Picture;

import java.util.List;

public interface PictureService extends IService<Picture> {

    boolean addPicture(Picture picture);

    boolean updatePicture(Picture picture);

    boolean deleteBatch(List<Integer> idList);

    boolean deleteOne(Integer id);

    List<Picture> getListByGalleryId(int galleryId);
}
