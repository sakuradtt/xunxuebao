package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.entity.Classify;

import java.util.List;

public interface ClassifyService extends IService<Classify> {

    List<Classify> getAllParentClassify();

    boolean addClassify(Classify classify);

    boolean updateClassify(Classify classify);

    boolean deleteBatch(List<Integer> idList);

    boolean deleteOne(Integer id);

    List<Classify> getClassifyByPid(Integer pid);

    boolean updateIconPath(Integer id,String iconPath);

    boolean updateHotRecommend(Integer id,int isHot);

    Classify getClassifyById(Integer id);

    List<Classify> queryClassifyAll(String industryName,int depth);
}
