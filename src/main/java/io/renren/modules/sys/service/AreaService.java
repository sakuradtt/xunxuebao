package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.sys.entity.Area;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AreaService extends IService<Area> {
    List<Area> getAllAreaTree();

    List<Area> getAreaByPid(int pid);
}
