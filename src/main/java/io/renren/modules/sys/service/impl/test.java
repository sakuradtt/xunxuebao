package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.ClassifyDao;
import io.renren.modules.sys.dao.DepartmentDao;
import io.renren.modules.sys.entity.Classify;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(rollbackFor = Exception.class)
public class test extends ServiceImpl<DepartmentDao, Department> {

    @Resource
    private DepartmentDao departmentDao;

    public List<Department> getAllParentDepartment() {
        List<Department> departmentList = departmentDao.selectList(null);
        //2、组装成父子的树型目录结构
        //2.1、先找到所有的一级行业分类
        List<Department> allParentDepartment = departmentList.stream()
                .filter(clazz -> clazz.getParentId()==0)
                .map(clazz ->
                {clazz.setChildren(getChildrens(clazz,departmentList));
                    return clazz;
                }).collect(Collectors.toList());
        return allParentDepartment;
    }

    public static void main(String[] args) {
        test test = new test();
        List<Department> departmentList = test.getAllParentDepartment();
        System.out.println(departmentList.toString());
    }


    //递归查找所有行业分类的子行业类
    private List<Department> getChildrens(Department root, List<Department> departmentList){
        List<Department> children = departmentList.stream()
                .filter(department -> {
                    //找到某个父分类下所有子分类
                    return department.getParentId().equals(root.getId());
                })
                .map(department -> {
                    department.setChildren(getChildrens(department, departmentList));
                    return department;
                })
                .collect(Collectors.toList());

        return children;
    }
}
