package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@TableName("teacher")
public class Teacher implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String tName;

    private Integer cid;
    @TableField(exist = false)
    private Course course;

    private Integer teachAge;
    private Integer gender;
    private String nation;
    private String teachStyle;
    private String avatar;
    private String introduce;
    private Date createTime;
    private Integer status;
    private String reason;

    private Integer companyId;
    @TableField(exist = false)
    private Company company;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;
}
