package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("school")
@ApiModel("校区实体类")
public class School implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private String principal;

    private String phone;

    private String address;

    private String lat;

    private String lng;

    private Integer companyId;

    @TableField(exist = false)
    private Company company;//外键，所属机构

    private Integer status;

    private String reason;

    private Date createTime;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;

}
