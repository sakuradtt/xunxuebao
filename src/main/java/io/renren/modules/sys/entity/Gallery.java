package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@TableName("gallery")
public class Gallery implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String title;
    private Integer picCount;

    private Integer companyId;
    @TableField(exist = false)
    private Company company;

    private Date createTime;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;
}
