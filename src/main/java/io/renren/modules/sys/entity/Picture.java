package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("picture")
public class Picture implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String title;
    private Integer galleryId;
    @TableField(exist = false)
    private Gallery gallery;

    private String picUrl;

    private Date createTime;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;
}
