package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@TableName("area")
public class Area implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;
    private String fullName;
    private Integer parentId;
    private Integer level;
    private String initial;
    private String pinyin;
    private String lng;
    private String lat;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @TableField(exist = false)
    private List<Area> children;

    @TableField(exist = false)
    private boolean hasChildren;
}
