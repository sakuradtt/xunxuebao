package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("course")
public class Course implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String cname;

    private String teacherMode;

    private String classType;

    private String serviceType;

    private String classHour;

    private String price;

    private String pics;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer schoolId;

    @TableField(exist = false)
    private School school;

    private Integer status;

    private String reason;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;

}
