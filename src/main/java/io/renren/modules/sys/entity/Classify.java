package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@TableName("classify")
@Data
public class Classify implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String industryName;

    private Integer parentId;

    private String iconPath;

    private Integer depth;

    private Integer sort;

    private Integer isHot;

    private Date createTime;

    private Date updateTime;

    private String parentAllId;

    @TableField(exist = false)
    private List<Classify> children;

    @TableLogic
    private Integer deleted;
}
