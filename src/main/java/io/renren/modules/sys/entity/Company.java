package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("company")
public class Company implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;
    private Integer classId;

    @TableField(exist = false)
    private Classify classify;

    private Integer areaId;

    @TableField(exist = false)
    private Area area;

    private String shortName;
    private String slogan;
    private String description;
    private String logo;
    private String bannerImgs;
    private String masterImgs;
    private String features;

    @TableLogic(value = "0",delval = "1")
    private Integer deleted;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Integer status;
    private Integer viewCount;

    private String classifyAll;

}
